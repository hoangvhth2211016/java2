package org.example;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{
        FileExplorer fe = new FileExplorer("F:\\");
        fe.createFile("text.txt", "Hello world");
        String fileContent = fe.enter("text.txt");
        System.out.println(fileContent);
        fe.createFolder("testFolder");
        fe.rename("testFolder","testFolder2");
        fe.deleteFile("text.txt");
        fe.deleteFile("testFolder2");
    }
}