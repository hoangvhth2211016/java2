package org.example;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileExplorer {
    private Path path;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public FileExplorer(String originalPath) {
        this.path = Paths.get(originalPath);
    }

    public List<File> list(){
        return Stream.of(Objects.requireNonNull(path.toFile().listFiles())).collect(Collectors.toList());
    }

    public String enter(String name) throws IOException{
        Path fPath = path.resolve(name);
        if(Files.exists(fPath)){
            if(Files.isDirectory(fPath)){
                path = fPath;
                return null;
            }
            if(!Files.isReadable(fPath)){
                return "File not readable";
            }
            return Files.readAllLines(fPath).toString();
        }
        throw new RuntimeException("File/Folder not found");
    }

    public void createFile(String name, String content) throws IOException {
        Path filePath = path.resolve(name);
        if(Files.exists(filePath)){
            throw new FileAlreadyExistsException("File already exist");
        }
        byte[] b = content.getBytes();
        Files.write(filePath, b);
        System.out.println("File created at "+filePath.toAbsolutePath());
    }

    public void createFolder(String name) throws IOException {
        Path folderPath = path.resolve(name);
        if(Files.exists(folderPath)){
            throw new FileAlreadyExistsException("File already exist");
        }
        Files.createDirectories(folderPath);
    }

    public void deleteFile(String name) throws IOException {
        Path fPath = path.resolve(name);
        boolean b = Files.deleteIfExists(fPath);
        if(!b){
            throw new RuntimeException("File/Folder not found");
        }
        System.out.println("File deleted");
    }

    public void rename(String currentName, String newName) throws IOException{
        Path currentPath = path.resolve(currentName);
        Path newPath = path.resolve(newName);
        if(Files.notExists(currentPath)){
            throw new RuntimeException("File/Folder not found");
        }
        Files.move(currentPath, newPath);
    }
}
