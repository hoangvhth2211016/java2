package org.example;

import lombok.Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Data
public class DbConfig {
    private static Connection connection;
    private static final String url = DBEnum.URL.getValue();
    private static final String user = DBEnum.USER.getValue();
    private static final String password = DBEnum.PASSWORD.getValue();
    private static final String driver = DBEnum.DRIVER.getValue();

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        if(connection == null){
        Class.forName(driver);
        connection =  DriverManager.getConnection(url, user, password);
        }
        return connection;
    }
}
