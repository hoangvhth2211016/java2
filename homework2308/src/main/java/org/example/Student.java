package org.example;

import lombok.Builder;
import lombok.Data;
import java.sql.Date;
import java.sql.Timestamp;


@Data
@Builder
public class Student {
    private Integer id;
    private String firstname;
    private String lastname;
    private String code;
    private String gender;
    private String schoolCode;
    private Date dob;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
