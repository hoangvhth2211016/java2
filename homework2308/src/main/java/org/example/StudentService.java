package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentService {
    public void insertOrUpdateStudents(Connection con, List<Student> studentList) throws SQLException {
        String query = "INSERT INTO students_vhhoang (first_name, last_name, code, gender, school_code, dob)\n" +
                "VALUES(?,?,?,?,?,?)\n" +
                "ON CONFLICT (code)\n" +
                "DO\n" +
                "UPDATE set " +
                "first_name = EXCLUDED.first_name, last_name = EXCLUDED.last_name," +
                "gender = EXCLUDED.gender, school_code = EXCLUDED.school_code, dob = EXCLUDED.dob, updated_at = ?";
        PreparedStatement statement = con.prepareStatement(query);
        for (Student st: studentList) {
            statement.setString(1,st.getFirstname());
            statement.setString(2,st.getLastname());
            statement.setString(3,st.getCode());
            statement.setString(4,st.getGender());
            statement.setString(5,st.getSchoolCode());
            statement.setDate(6,st.getDob());
            statement.setTimestamp(7, new Timestamp(new Date().getTime()));
            statement.addBatch();
        }
        int [] rows = statement.executeBatch();
        System.out.println(rows.length);
        statement.close();
    }

    public List<Student> getOlderStudent(Connection con) throws SQLException {
        String query = "SELECT * FROM students_vhhoang WHERE AGE(current_date, dob) >= interval '18 years'";
        List<Student> students = new ArrayList<>();
        PreparedStatement statement = con.prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while(rs.next()){
            Student st = Student.builder()
                    .id(rs.getInt("id"))
                    .firstname(rs.getString("first_name"))
                    .lastname(rs.getString("last_name"))
                    .code(rs.getString("code"))
                    .gender(rs.getString("gender"))
                    .schoolCode(rs.getString("school_code"))
                    .dob(rs.getDate("dob"))
                    .createdAt(rs.getTimestamp("created_at"))
                    .updatedAt(rs.getTimestamp("updated_at"))
                    .build();
            students.add(st);
        }
        statement.close();
        return students;
    }
}
