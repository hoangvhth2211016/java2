package org.example;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class CsvReader {

    private static CSVParser readCsv(String[] headers,String fileName) throws IOException {
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder().setHeader(headers).setSkipHeaderRecord(true).build();
        return csvFormat.parse(new FileReader(fileName));
    }

    public static List<Student> readStudents(String fileName) throws IOException {
        String [] headers = {"#", "First Name", "Last Name", "Code", "Gender", "School Code", "DOB"};
        CSVParser records = readCsv(headers, fileName);
        List<Student> students = new ArrayList<>();
        records.forEach(record -> {
            Student st = Student.builder()
                    .id(parseInt(record.get("#")))
                    .firstname(record.get("First Name"))
                    .lastname(record.get("Last Name"))
                    .code(record.get("Code"))
                    .gender(record.get("Gender"))
                    .schoolCode(record.get("School Code"))
                    .dob(Date.valueOf(record.get("DOB")))
                    .build();
            students.add(st);
        });
        return students;
    }

    public static List<School> readSchools(String fileName) throws IOException {
        String [] headers = {"#", "Name", "Code", "Address"};
        CSVParser records = readCsv(headers, fileName);
        List<School> schools = new ArrayList<>();
        records.forEach(record -> {
            School st = School.builder()
                    .id(parseInt(record.get("#")))
                    .name(record.get("Name"))
                    .code(record.get("Code"))
                    .address(record.get("Address"))
                    .build();
            schools.add(st);
        });
        return schools;
    }
}
