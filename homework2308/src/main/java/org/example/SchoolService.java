package org.example;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class SchoolService {
    public void insertSchools(Connection connection, List<School> schools) throws SQLException {
        String sql = "INSERT INTO schools_vhhoang (name, code, address) VALUES ( ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        for (School school : schools) {
            statement.setString(1, school.getName());
            statement.setString(2, school.getCode());
            statement.setString(3, school.getAddress());
            statement.addBatch();
        }
        int [] rows = statement.executeBatch();
        System.out.println(rows.length);
        statement.close();
    }
}
