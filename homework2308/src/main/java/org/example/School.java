package org.example;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class School {
    private Integer id;
    private String name;
    private String code;
    private String address;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
