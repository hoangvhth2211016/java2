package org.example;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


public class Main {

    private static final StudentService studentService = new StudentService();
    private static final SchoolService schoolService = new SchoolService();

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        Connection con = DbConfig.getConnection();

        List<Student> students = CsvReader.readStudents("Student.csv");
        List<School> schools = CsvReader.readSchools("School.csv");
        List<Student> updateStudents = CsvReader.readStudents("Student_update.csv");

        con.setAutoCommit(false);
        schoolService.insertSchools(con, schools);
        studentService.insertOrUpdateStudents(con, students);
        studentService.insertOrUpdateStudents(con, updateStudents);
        con.commit();

        List<Student> searchStudents = studentService.getOlderStudent(con);
        System.out.println(searchStudents.size());

        con.close();
    }
}
