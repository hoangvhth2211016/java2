package org.example;

import lombok.*;

@Getter
@RequiredArgsConstructor
public enum DBEnum {
    URL("jdbc:postgresql://4.194.217.58:5432/t2301e"),
    USER("t2301e"),
    PASSWORD("t2301e"),
    DRIVER("org.postgresql.Driver");
    private final String value;
}
