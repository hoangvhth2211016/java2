package org.example;

import java.io.IOException;
import java.util.List;

public class Main {


    public static void main(String[] args) throws IOException {
        final PostService postService = new PostService();

        List<Post> posts = postService.list(0, 10);
        System.out.println(posts);

        Post post = new Post();
        post.setTitle("demo");
        post.setBody("demo");
        post.setUserId(2);
        post.setTags(new String[]{"demo1", "demo2", "demo3"});

        Post newPost = postService.create(post);
        System.out.println(newPost);

        Post updatePost = postService.update(1, post);
        System.out.println(updatePost);

        Post deletedPost = postService.delete(1);
        System.out.println(deletedPost);
    }
}