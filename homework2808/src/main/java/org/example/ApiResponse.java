package org.example;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.*;

import java.util.*;

@Data
public class ApiResponse<T> {
    private Map<String, List<T>> data = new LinkedHashMap<>();
    private int total;
    private int skip;
    private int limit;

    @JsonAnySetter
    public void setData(String key, List<T> value){
        data.put(key, value);
    }
}
