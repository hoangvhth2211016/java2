package org.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {
    private Integer id;
    private String title;
    private String body;
    private Integer userId;
    private String[] tags;
    private int reactions;
}
