package org.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import okhttp3.*;

import java.io.IOException;
import java.util.List;
@Data
public class PostService {

    private static final String BASE_URL = "https://dummyjson.com/posts";

    private static final OkHttpClient client = new OkHttpClient();

    private static final MediaType JSON = MediaType.parse("application/json");

    private static final ObjectMapper mapper = new ObjectMapper();


    public List<Post> list(int page, int size) throws IOException {
        if(page<1) page = 1;
        Request req = new Request
                .Builder()
                .url(String.format("%s?limit=%d&skip=%d", BASE_URL, size, page*size))
                .build();
        String resString = client.newCall(req).execute().body().string();
        JavaType javaType = mapper.getTypeFactory().constructParametricType(ApiResponse.class, Post.class);
        ApiResponse<Post> apiResponse =  mapper.readValue(resString, javaType);
        return apiResponse.getData().get("posts");
    }

    public Post create(Post post) throws IOException {
        String jsonString = mapper.writeValueAsString(post);
        RequestBody body = RequestBody.create(jsonString, JSON);
        Request req = new Request
                .Builder()
                .url(BASE_URL+"/add")
                .post(body)
                .build();
        Response response = client.newCall(req).execute();
        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            System.out.println("Post created: ");
            return mapper.readValue(responseBody, new TypeReference<Post>(){});
        } else {
            System.out.println("Request failed with code: " + response.code());
            return null;
        }
    }

    public Post update(int id, Post post) throws IOException {
        post.setId(id);
        String jsonString = mapper.writeValueAsString(post);
        RequestBody body = RequestBody.create(jsonString, JSON);
        Request req = new Request
                .Builder()
                .url(BASE_URL+"/"+id)
                .put(body)
                .build();
        Response response = client.newCall(req).execute();
        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            System.out.println("Post updated: ");
            return mapper.readValue(responseBody, new TypeReference<Post>(){});
        } else {
            System.out.println("Request failed with code: " + response.code());
            return null;
        }
    }

    public Post delete(int id) throws IOException {
        Request req = new Request
                .Builder()
                .url(BASE_URL+"/"+id)
                .delete()
                .build();
        Response response = client.newCall(req).execute();
        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            System.out.println("Post deleted: ");
            return mapper.readValue(responseBody, new TypeReference<Post>(){});
        } else {
            System.out.println("Request failed with code: " + response.code());
            return null;
        }
    }
}
