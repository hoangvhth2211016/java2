package org.example;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
public class FileThread extends Thread{
    private String file;
    private final List<Integer> sums;

    @Override
    @SneakyThrows
    public void run() {
        synchronized (sums){
            sums.add(Main.sum(file));
        }
    }
}
