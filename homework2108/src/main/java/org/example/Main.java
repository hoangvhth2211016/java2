package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class Main{

    public static Integer sum(String file) throws IOException {
        return Files.readAllLines(Path.of(file)).stream().mapToInt(Integer::parseInt).sum();
    }

    public static void main(String[] args) throws InterruptedException {
        List<String> files = List.of("file1.txt","file2.txt", "file3.txt");

        //Su dung thread
        List<FileThread> ftList = new ArrayList<>();
        List<Integer> sum1 = new ArrayList<>();
        files.forEach(file ->{
            FileThread ft = new FileThread(file, sum1);
            ftList.add(ft);
            ft.start();
        });
        ftList.forEach(ft -> {
            try {
                ft.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        System.out.println("sum1: "+sum1);

        //Su dung CompletableFuture
        List<CompletableFuture<Integer>> cfs = new ArrayList<>();
        for(String file: files){
            CompletableFuture<Integer> cf = CompletableFuture.supplyAsync(() ->{
                try {
                    return sum(file);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            cfs.add(cf);
        }
        CompletableFuture<List<Integer>> allCf = CompletableFuture.allOf(cfs.toArray(new CompletableFuture[0]))
                .thenApply(v -> cfs.stream().map(CompletableFuture::join).toList());

        List<Integer> sum2 = allCf.join();
        System.out.println("sum2: "+sum2);

        // Su dung parallelStream
        List<Integer> sum3 = files.parallelStream().map(file ->{
            try {
                return sum(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).toList();
        System.out.println("sum3: "+sum3);
    }


}