import java.time.LocalDate;
import java.time.LocalDateTime;

public class InvalidDobException extends CommonException{
    public InvalidDobException(LocalDate dob) {

        super(200, "Year \""+dob.getYear()+"\" is not valid");
    }
}
