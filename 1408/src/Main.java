import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
    private static final ArrayList<Student> students = new ArrayList<Student>();

    private static void addStudent(String name, String gender, LocalDate dob){
        try{
            Student s = new Student(name, gender, dob);
            students.add(s);
        }catch(CommonException e){
            System.out.println("Error: "+e.getErrorCode()+", Year of Birth: "+e.getExtraInfo());
        }
    }

    public static void main(String[] args) {
        addStudent("hoang", "male", LocalDate.of(2012, 5,20));
        students.forEach(student -> System.out.println(student.toString()));
    }
}