import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.regex.Pattern;

public class Student {
    private String name;
    private String gender;
    private LocalDate dob;

    public Student(String name, String gender, LocalDate dob) {
        Pattern pattern = Pattern.compile("^([a-zA-Z]+\\s*)+$");
        if(!pattern.matcher(name).matches()){
            throw new InvalidNameException(name);
        }else{
        this.name = name;
        }
        this.gender = gender;
        if(dob.getYear()<2006 || dob.getYear()>2017){
            throw new InvalidDobException(dob);
        }
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", dob=" + dob +
                '}';
    }

}
