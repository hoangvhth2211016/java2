import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Database {
    private final List<Record> recordList = new ArrayList<>();

    public Database() {
    }

    public List<Record> getRecordList() {
        return recordList;
    }

    public Record find(Integer id){
        return recordList.stream().filter(record -> record.getId().equals(id)).findAny().orElse(null);
    }

    public void save(Record record){
        Record rec = find(record.getId());
        if(rec != null){
            record.setCreatedAt(rec.getCreatedAt());
            recordList.set(recordList.indexOf(rec), record);
        }else if(record.getId()==null){
            int nextId = recordList.stream().mapToInt(Record::getId).max().orElse(-1);
            record.setId(++nextId);
            recordList.add(record);
        }else{
            recordList.add(record);
        }
    }

    public Record delete(Integer id){
        Record rec = find(id);
        if(rec!=null){
            recordList.remove(rec);
            return rec;
        }
        return null;
    }

    public List<Record> findByCreatedAtAfter(ZonedDateTime time){
        return recordList.stream().filter(rec -> rec.getCreatedAt().isAfter(time) || rec.getCreatedAt().isEqual(time)).collect(Collectors.toList());
    }

    public List<Record> findByUpdatedAtAfter(ZonedDateTime time){
        return recordList.stream().filter(rec -> rec.getUpdatedAt().isAfter(time) || rec.getUpdatedAt().isEqual(time)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Database{\n" +
                "recordList:\n" + recordList +
                '}';
    }
}
