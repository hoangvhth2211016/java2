import java.time.LocalDate;
import java.time.ZonedDateTime;

public class Main {
    public static void main(String[] args) {
        Database database = new Database();

        Student st1 = new Student(null, "John", LocalDate.of(1994, 5,20));
        Student st2 = new Student(null, "Alice", LocalDate.of(2000, 2,2));
        School sc1 = new School(null, "ABC School", "123 Main St");
        School sc2 = new School(null, "XYZ School", "456 Elm St");

        database.save(st1);
        database.save(st2);
        database.save(sc1);
        database.save(sc2);

        System.out.println(database);
        Student s1Update = new Student(0, "Adam", LocalDate.of(2001,1, 21));
        database.save(s1Update);
        System.out.println(database);

        Student s5 = new Student(8, "Teth", LocalDate.of(1998,5, 1));
        database.save(s5);
        System.out.println(database);

        Student s4 = new Student(null, "Dave", LocalDate.of(1993,10, 20));
        database.save(s4);
        System.out.println(database);

        Record del = database.delete(9);
        if(del == null){
            System.out.println("Record does not exist in database!");
        }else{
            System.out.println("Record deleted successfully: "+del);
        }
        System.out.println(database);

        System.out.println("Timestamp: "+ZonedDateTime.now());
        System.out.println(database.findByCreatedAtAfter(ZonedDateTime.now().minusNanos(10000000)));
        System.out.println("Timestamp: "+ZonedDateTime.now());
        System.out.println(database.findByUpdatedAtAfter(ZonedDateTime.now().minusNanos(10000000)));
    }
}